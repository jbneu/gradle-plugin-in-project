import xyz.jbneu.gradle.ModelVersion

plugins {
    xyz.jbneu.gradle.greeting
    xyz.jbneu.gradle.devdb
}

dependencies {
}

myExtension {
    b.set("JB")
}

versionDBAProof {
    targetModelVersion.set(ModelVersion(1, 2, 3))
}
