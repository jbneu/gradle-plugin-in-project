import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("java-gradle-plugin")
    `kotlin-dsl`
}

gradlePlugin {
    plugins {
        create("gradle-plugin-greeting") {
            id = "xyz.jbneu.gradle.greeting"
            implementationClass = "xyz.jbneu.gradle.GreetingPlugin"
        }
    }
    plugins {
        create("gradle-plugin-devdb") {
            id = "xyz.jbneu.gradle.devdb"
            implementationClass = "xyz.jbneu.gradle.VersionDBAProofPlugin"
        }
    }
}

kotlinDslPluginOptions {
    experimentalWarning.set(false)
}

dependencies {
    implementation(kotlin("gradle-plugin"))
    implementation("org.ajoberstar.grgit:grgit-core:3.1.1")
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://dl.bintray.com/ajoberstar/maven")
    }
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}