package xyz.jbneu.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.plugins.BasePlugin
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Delete
import org.gradle.kotlin.dsl.*

open class MyConfigExtension(objects: ObjectFactory) {
    var a: Int = 0
    val b: Property<String> = objects.property()
}

open class GreetingPlugin : Plugin<Project> {
    override fun apply(project: Project) {

        project.apply<BasePlugin>()

        val config = project.extensions.create<MyConfigExtension>("myExtension")
        val cleanTask by project.tasks.named<Delete>("clean")

        val helloTask by project.tasks.register("hello") {
            description = "Polite plugin"
            val b: Property<String> = config.b.convention("world")

            doLast {
                println("Hello ${b.get()} from the GreetingPlugin")
            }
        }

        cleanTask.dependsOn(helloTask)

        project.tasks.register("hello2") {
            description = "Polite plugin"
            doLast {
                println("Hello from the GreetingPlugin task 2")
            }
        }
    }
}