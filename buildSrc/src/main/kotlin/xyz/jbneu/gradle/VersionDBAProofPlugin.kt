package xyz.jbneu.gradle

import org.ajoberstar.grgit.Grgit
import org.gradle.api.DefaultTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.property
import org.gradle.kotlin.dsl.register

//-------------------------------------------------
// Task AutoVersion
//-------------------------------------------------

open class FullAutoVersionTask : DefaultTask() {

    @Input
    val targetModelVersion: Property<ModelVersion> = project.objects.property()

    @Internal
    val gitHelper: Property<GitHelper> = project.objects.property()

    // trick to be tested
    private val git: GitHelper by lazy { gitHelper.get() }

    @TaskAction
    fun doAutoVersion() {
        val targetModel = targetModelVersion.get()
        logger.debug("targetModel: $targetModel")

        val tags = git.findTagsMatchingModelVersion(targetModel)
        logger.debug("git tags found: ${tags.joinToString(", ")}")

        val nextDevVersion = nextDevVersion(targetModel, tags)
        logger.debug("Working version will be $nextDevVersion")

        val existingBranch = git.findReleaseBranchMatchingVersion(nextDevVersion)

        if (existingBranch != null) {
            checkoutAndUpdateReleaseBranch(existingBranch)
        } else {
            createNewReleaseBranch(nextDevVersion)
        }
    }

    private fun nextDevVersion(targetModel: ModelVersion, tags: List<GitTag>): Version {
        return tags.map { it.version }
                .filter { it.modelVersion == targetModel }
                .max()?.withIncDevVersion()
                ?: Version(targetModel)
    }

    private fun checkoutAndUpdateReleaseBranch(existingReleaseBranch: GitReleaseBranch) {
        logger.debug("Existing release branch '$existingReleaseBranch' will be used.")
        git.checkoutAndPull(existingReleaseBranch)
        // TODO check that version in gradle.properties is aligned
    }

    private fun createNewReleaseBranch(version: Version) {
        val newGitReleaseBranch = GitReleaseBranch(version)
        logger.debug("New release branch '$newGitReleaseBranch' will be created and used.")
        git.createReleaseBranchFromMaster(newGitReleaseBranch)
        // TODO set targetModelVersion in gradle.properties
    }
}

//-------------------------------------------------
// plugin
//-------------------------------------------------

open class VersionDBAProofExtension(objectFactory: ObjectFactory) {
    val targetModelVersion: Property<ModelVersion> = objectFactory.property()
}

open class VersionDBAProofPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        val gitHelper = GitHelper(project)
        val extension = project.extensions.create<VersionDBAProofExtension>("versionDBAProof")
        conventions(extension, project)

        project.tasks.register<FullAutoVersionTask>("autoVersion") {
            targetModelVersion.set(extension.targetModelVersion)
            this.gitHelper.set(gitHelper)
        }
    }

    private fun conventions(extension: VersionDBAProofExtension, project: Project) {
        if (project.hasProperty("targetModelVersion")) {
            extension.targetModelVersion.convention(ModelVersion.parse(project.properties["targetModelVersion"] as String))
        }
    }
}


//-------------------------------------------------
// git
//-------------------------------------------------

class GitHelper(project: Project) {
    private val git: Grgit = Grgit.open(mapOf("dir" to project.projectDir))

    init {
        git.fetch(mapOf("tagMode" to "all", "prune" to true))
    }

    private val tags: List<GitTag> by lazy { retrieveTags() }
    private val releaseBranches: List<GitReleaseBranch> by lazy { retrieveBranches() }

    fun findTagsMatchingModelVersion(version: ModelVersion): List<GitTag> {
        return tags.filter { it.version.modelVersion == version }
    }

    fun findReleaseBranchMatchingVersion(version: Version): GitReleaseBranch? {
        return releaseBranches.find { it.version == version }
    }

    private fun retrieveBranches(): List<GitReleaseBranch> {
        return git.branch.list()
                .map { it.name }
                .filter { it.startsWith(GitReleaseBranch.releaseBranchPrefix) }
                .map { GitReleaseBranch.parse(it) }
    }

    private fun retrieveTags(): List<GitTag> {
        return git.tag.list()
                .map { it.name }
                .filter { it.startsWith(GitTag.prefix) }
                .map { GitTag.parse(it) }
    }

    fun checkoutAndPull(releaseBranch: GitReleaseBranch) {
        git.checkout(mapOf("branch" to releaseBranch.branchName))
        git.pull()
    }

    fun createReleaseBranchFromMaster(releaseBranch: GitReleaseBranch) {
        git.checkout(mapOf("branch" to "master"))
        git.pull()
        git.checkout(mapOf("branch" to releaseBranch.branchName, "createBranch" to true))
        git.push(mapOf("refsOrSpecs" to listOf(releaseBranch.branchName)))
    }
}

data class GitReleaseBranch(val version: Version) {
    companion object {
        const val releaseBranchPrefix = "release"
        fun parse(branchName: String): GitReleaseBranch {
            val pattern = Regex("""$releaseBranchPrefix/(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-([1-9]\d*))?""")
            val result = pattern.matchEntire(branchName) ?: throw IllegalArgumentException(
                    """Version format '$branchName' for name not allowed.
                   |Expected format: '$pattern'""".trimMargin()
            )
            return GitReleaseBranch(Version(
                    result.groupValues[1].toInt(),
                    result.groupValues[2].toInt(),
                    result.groupValues[3].toInt(),
                    if (result.groupValues[4].isEmpty()) 0 else result.groupValues[4].toInt()
            ))
        }
    }
    val branchName: String by lazy {
        "$releaseBranchPrefix/$version"
    }

    override fun toString(): String {
        return branchName
    }
}

data class GitTag(val version: Version) {
    companion object {
        const val prefix = "v"
        fun parse(tagName: String): GitTag {
            val pattern = Regex("""v(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-([1-9]\d*))?""")
            val result = pattern.matchEntire(tagName) ?: throw IllegalArgumentException(
                    """Version format '$tagName' for name not allowed.
                   |Expected format: 'v[x].[y].[z][-A]?'""".trimMargin()
            )
            val version = Version(
                    result.groupValues[1].toInt(),
                    result.groupValues[2].toInt(),
                    result.groupValues[3].toInt(),
                    if (result.groupValues[4].isEmpty()) 0 else result.groupValues[4].toInt()
            )
            return GitTag(version)
        }
    }

    override fun toString(): String {
        return "$prefix$version"
    }
}

//-------------------------------------------------
// Version
//-------------------------------------------------

data class ModelVersion(val major: Int,
                        val minor: Int,
                        val patch: Int) : Comparable<ModelVersion> {

    companion object {
        fun parse(version: String): ModelVersion {
            val pattern = Regex("""(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)""")
            val result = pattern.matchEntire(version) ?: throw IllegalArgumentException(
                    """Version format '$version' not allowed.
                   |Expected format: '[x].[y].[z]'""".trimMargin()
            )
            return ModelVersion(
                    result.groupValues[1].toInt(),
                    result.groupValues[2].toInt(),
                    result.groupValues[3].toInt()
            )
        }
    }

    init {
        // TODO targetModelVersion 0.0.0 not allowed
        // TODO major, minor, patch < 0 not allowed
    }

    override fun compareTo(other: ModelVersion): Int {
        return when {
            major > other.major -> 1
            major < other.major -> -1
            minor > other.minor -> 1
            minor < other.minor -> -1
            patch > other.patch -> 1
            patch < other.patch -> -1
            else -> 0
        }
    }

    override fun toString(): String {
        return "$major.$minor.$patch"
    }
}

data class Version(val modelVersion: ModelVersion, val devVersion: Int = 0) : Comparable<Version> {
    companion object {
        fun parse(version: String): Version {
            val pattern = Regex("""(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-([1-9]\d*))?""")
            val result = pattern.matchEntire(version) ?: throw IllegalArgumentException(
                    """Version format '$version' not allowed.
                   |Expected format: '[x].[y].[z][-A]?'""".trimMargin()
            )
            return Version(
                    result.groupValues[1].toInt(),
                    result.groupValues[2].toInt(),
                    result.groupValues[3].toInt(),
                    if (result.groupValues[4].isEmpty()) 0 else result.groupValues[4].toInt()
            )
        }
    }

    constructor(major: Int,
                minor: Int,
                patch: Int,
                devVersion: Int) : this(ModelVersion(major, minor, patch), devVersion)

    init {
        // TODO devVersion < 0 not allowed
    }

    fun withIncDevVersion(): Version {
        return this.copy(devVersion = devVersion + 1)
    }

    override fun compareTo(other: Version): Int {
        val modelComparison = modelVersion.compareTo(other.modelVersion)
        return if (modelComparison == 0) {
            when {
                devVersion > other.devVersion -> 1
                devVersion < other.devVersion -> -1
                else -> 0
            }
        } else {
            modelComparison
        }
    }

    override fun toString(): String {
        val dv = if (devVersion > 0) "-$devVersion" else ""
        return "$modelVersion$dv"
    }
}